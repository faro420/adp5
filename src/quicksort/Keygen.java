package quicksort;

import java.util.ArrayList;

public class Keygen {
    private ArrayList<Integer> list;

    public Keygen(int start, int end,int size) {
        list = new ArrayList<Integer>(size);
        int counter = 0;
        int puffer = 0;
        while (counter != size) {
            puffer = random(start, end);
            if (!list.contains(puffer)) {
                list.add(puffer);
                counter++;
            }
        }
    }

    public ArrayList<Integer> getList() {
        return list;
    }

    private int random(int left, int right) {
        return left+(int) (Math.random() * (right - left));
    }
}
