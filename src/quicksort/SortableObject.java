package quicksort;

public class SortableObject
{
    private int _key;
    private Object _data;
    
    public SortableObject(int key)
    {
        _key = key;
        
    }
    
    public int getKey()
    {
        return _key;
    }
    
    public Object getData()
    {
        return _data;
    }
    public String toString()
    {
        return "" + _key;
    }
}
