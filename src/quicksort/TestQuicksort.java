package quicksort;

import java.util.Random;


public class TestQuicksort
{

    public static void main(String[] args)
    {
        auswertung(10);
        auswertung(100);
        auswertung(1000);
        auswertung(10000);
        auswertung(100000);
        auswertung(1000000);
        
    }
    private static void auswertung(int n)
    {
        int compcounter = 0;
        int movcounter = 0;
        QuicksortImpl test = new QuicksortImpl();
        SortableObject[] array = new SortableObject[n];
        for(int i = 0; i < array.length; i++)
        {
            array[i] = new SortableObject(randomNumber(1,n));
            //array[i] = new SortableObject(i + 1); //worst case
        }
        
        test.quicksort(0, array.length - 1, array);
        compcounter = test.getCCounter();
        movcounter = test.getMCounter();
        
        System.out.printf(" N = %d C = %d , M = %d \n",n , compcounter, movcounter);
    }
    private static int randomNumber(int min, int max)
    {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

}
